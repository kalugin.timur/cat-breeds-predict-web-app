from flask import Flask, jsonify, request
from flask_cors import CORS

from prediction import run_predict

JSONIFY_PRETTYPRINT_REGULAR = True
JSON_SORT_KEYS = False

app = Flask(__name__)
app.config.from_object(__name__)

CORS(app)


@app.route('/predict', methods=['POST'])
def predict():
    image_bs64 = request.json['image_bs64']
    if image_bs64 is not None:
        return jsonify(run_predict(image_bs64))


if __name__ == '__main__':
    app.run()
