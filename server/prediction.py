import numpy as np
import torch
import torchvision.transforms as transforms
import torch.nn.functional as functional
import re
import base64

from io import BytesIO
from PIL import Image
from torch.autograd import Variable

WORK_FOLDER = 'model/'

gpu = torch.cuda.is_available()

if not gpu:
    device = torch.device("cpu")
else:
    device = torch.device("cuda:0")


def process_image(image):
    """ Scales, crops, and normalizes a PIL image for a PyTorch model,
        returns an Numpy array
    """
    # Process a PIL image for use in a PyTorch model
    # tensor.numpy().transpose(1, 2, 0)
    preprocess = transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(224),
        transforms.ToTensor(),
        transforms.Normalize(mean=[0.485, 0.456, 0.406],
                             std=[0.229, 0.224, 0.225])
    ])
    image = preprocess(image)
    return image


def run_predict(image_bs64):
    image_bs64 = re.sub('^data:image/.+;base64,', '', image_bs64)
    image_decode = base64.b64decode(image_bs64)
    image_bytes = BytesIO(image_decode)
    image_file = Image.open(image_bytes).convert('RGB')
    transformed_img = process_image(image_file)
    return predict(transformed_img)


def predict(image, topk=5):
    img = np.expand_dims(image, 0)
    img = torch.from_numpy(img)
    inputs = Variable(img).to(device)

    prediction_model_path = WORK_FOLDER + 'checkpoints/cat_breeds_cnn.pth'
    model = torch.load(prediction_model_path)
    model.eval()

    logits = model.forward(inputs)

    ps = functional.softmax(logits, dim=1)
    topk = ps.cpu().topk(topk)
    values, indices = (e.data.numpy().squeeze().tolist() for e in topk)
    breed_names = []
    for breed_id in indices:
        breed_names.append(
            list(model.class_to_idx.keys())[list(model.class_to_idx.values()).index(breed_id)]
        )
    breed_to_pred = dict(zip(breed_names, values))
    return breed_to_pred
