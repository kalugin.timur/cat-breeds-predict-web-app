import Vue from 'vue';
import Router from 'vue-router';
import Prediction from '@/components/Prediction';
import ImageLoad from '@/components/ImageLoad';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'ImageLoad',
      component: ImageLoad,
    },
  ],
  mode: 'history',
});
